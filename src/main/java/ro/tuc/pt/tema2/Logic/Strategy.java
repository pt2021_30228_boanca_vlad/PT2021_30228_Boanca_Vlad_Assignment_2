package ro.tuc.pt.tema2.Logic;

import ro.tuc.pt.tema2.DataModels.Client;
import ro.tuc.pt.tema2.DataModels.Queue;
import java.util.List;

public interface Strategy {
    public void addClient(List<Queue> cozi, Client c, int maxClientsPerQueue);
}
