package ro.tuc.pt.tema2.Logic;

import ro.tuc.pt.tema2.DataModels.Client;
import ro.tuc.pt.tema2.DataModels.Queue;

import java.util.List;

public class StrategyQueue implements Strategy{

    @Override
    public void addClient(List<Queue> cozi, Client c, int maxClientsPerQueue) {
        Queue bestQueue = cozi.get(0);
        for(Queue q : cozi) {
            if(q.getClients().size() < bestQueue.getClients().size() && q.getClients().size() < maxClientsPerQueue)
                bestQueue = q;
        }
        bestQueue.addClient(c);
    }
}
