package ro.tuc.pt.tema2.Logic;

import ro.tuc.pt.tema2.DataModels.Client;
import ro.tuc.pt.tema2.DataModels.Queue;
import ro.tuc.pt.tema2.DataModels.Scheduler;
import ro.tuc.pt.tema2.GUI.SimulationFrame;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class SimulationManager implements Runnable{

    private int timeLimit;
    private int minArrivalTime;
    private int maxArrivalTime ;
    private int minServiceTime;
    private int maxServiceTime;
    private int numberOfQueues;
    private int numberOfClients;
    private int peakHour, peakTime;
    private int currentTime;
    private Scheduler.SelectionPolicy selectionPolicy;
    private SimulationFrame frame;
    private Scheduler scheduler;
    private List<Client> generatedClients;

    public SimulationManager() {
        frame = new SimulationFrame();
        frame.getSubmitSettingsButton().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.prepareForDisplaying();
                getValues();
                generateRandomClients();
                scheduler = new Scheduler(numberOfQueues, numberOfClients);
                scheduler.changeStrategy(Scheduler.SelectionPolicy.SHORTEST_TIME);
                startSimulation();
            }
        });
    }

    private void generateRandomClients() {
        generatedClients = new LinkedList<>();
        Random random = new Random();
        for(int i = 0; i < numberOfClients; i++) {
            generatedClients.add(new Client(i, random.nextInt(maxArrivalTime - minArrivalTime) + minArrivalTime, random.nextInt(maxServiceTime - minServiceTime) + minServiceTime));
        }
        Collections.sort(generatedClients);
    }

    @Override
    public void run() {
        try {
            runSimulation();
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }
    }

    private void runSimulation() throws IOException, InterruptedException {
        peakHour = 0; peakTime = 0; currentTime = 0;
        new File("logs.txt").delete();
        BufferedWriter writer = new BufferedWriter(new FileWriter("logs.txt", true));
        StringBuilder output;
        while(currentTime < timeLimit) {
            output = new StringBuilder();
            output.append(dispatchAndGetWaitingClients(currentTime)).append("\n");
            output.append(getClientsFromQueues(currentTime));
            writer.append(output).append("\n");
            updatePeakHour();
            currentTime++;
            if(generatedClients.isEmpty() && allQueuesAreEmpty())
                break;
            frame.updateCurrentTime(currentTime);
            Thread.sleep(1000);
        }
        String statistics = getAverageStatistics();
        writer.append(statistics);
        writer.flush();
        writer.close();
        frame.updateWaitingClients(statistics);
        frame.getWaitingClients().setForeground(Color.green);
    }
    private String dispatchAndGetWaitingClients(int currentTime) {
        Client client;
        String output ="Time " + currentTime + "\n";
        String waitingClients = "Waiting clients: ";
        for(int i = 0; i < generatedClients.size(); i++) {
            client = generatedClients.get(i);
            if(client.getArrivalTime() == currentTime) {
                scheduler.dispatchClient(client);
                generatedClients.remove(client);
                i--;
            } else if(client.getArrivalTime() > currentTime) {
                waitingClients += client + "; ";
            }
        }
        frame.updateWaitingClients(waitingClients);
        output += waitingClients;
        return output;
    }
    private String getAverageStatistics() {
        double avgWaitingTime = 0.0, avgServiceTime = 0.0;
        for(Queue q : scheduler.getQueues()) {
            avgServiceTime += q.getTotalServiceTime();
            avgWaitingTime += q.getTotalWaitingTime();
            q.cancel();
        }
        avgWaitingTime = avgWaitingTime / numberOfClients;
        avgServiceTime = avgServiceTime / numberOfClients;
        DecimalFormat df = new DecimalFormat("#.00");
        return "Average waiting time: " + df.format(avgWaitingTime) + " seconds.\n" + "Average service time: " + df.format(avgServiceTime) + " seconds.\n"
                + "Peak moment: " + peakTime + "\n";
    }
    private String getClientsFromQueues(int currentTime) {
        String output = "";
        String queueContent;
        StringBuilder forGUI;
        int ind = 1;
        for(Queue q : scheduler.getQueues()) {
            forGUI = new StringBuilder();
            output += ("Queue " + (ind++) + ": ");
            queueContent = "";
            for(Client c : q.getClients()) {
                forGUI.append(c + "\n");
                queueContent += c + "; ";
            }
            queueContent += "\n";
            if(q.isEmpty() || currentTime < minArrivalTime) {
                output += "closed\n";
                frame.updateQueue("closed\n", ind - 1);
            }
            else {
                output += queueContent;
                frame.updateQueue(forGUI.toString(), ind - 1);
            }
        }
        return output;
    }
    private void updatePeakHour() {
        int p = 0;
        for(Queue q : scheduler.getQueues()) {
            p += q.getClients().size();
        }
        if(p >= peakHour) {
            peakHour = p;
            peakTime = currentTime;
        }
    }
    private void getValues() {
        timeLimit = frame.getTimeLimitValue();
        minArrivalTime = frame.getMinArrivalTimeValue();
        maxArrivalTime = frame.getMaxArrivalTimeValue();
        minServiceTime = frame.getMinServiceTimeValue();
        maxServiceTime = frame.getMaxServiceTimeValue();
        numberOfClients = frame.getNumberOfClientsValue();
        numberOfQueues = frame.getNumberOfQueuesValue();
        selectionPolicy = frame.getStrategy();
    }
    private void startSimulation() {
        Thread t = new Thread(this);
        t.start();
    }
    private boolean allQueuesAreEmpty() {
        for(Queue q : scheduler.getQueues())
            if(!q.isEmpty())
                return false;
        return true;
    }
}
