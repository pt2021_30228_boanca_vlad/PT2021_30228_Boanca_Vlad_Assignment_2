package ro.tuc.pt.tema2.GUI;

import com.jgoodies.forms.builder.PanelBuilder;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import ro.tuc.pt.tema2.DataModels.Scheduler;

import javax.swing.*;
import java.awt.*;

public class SimulationFrame extends JFrame {
    private JTabbedPane mainPanel = new JTabbedPane();
    private JPanel simulationPanel = new JPanel();
    private JPanel displayingPanel = new JPanel();
    private final JPanel timeOfSimulationPanel = new JPanel();
    private JTextArea waitingClients;
    private JTextArea[] clients;
    private JTextField numberOfQueuesField;
    private JTextField numberOfClientsField;
    private JTextField timeLimitField;
    private JTextField minArrivalTimeField;
    private JTextField maxArrivalTimeField;
    private JTextField minServiceTimeField;
    private JTextField maxServiceTimeField;
    private JComboBox selectStrategy;
    private JButton submitSettingsButton;

    public SimulationFrame() {
        createGUI();
    }

    private void updateDisplayingPanel() {
        displayingPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1; c.weighty = 1; c.gridy = 0; c.gridwidth = 2; c.gridheight = 1;
        JPanel[] qs = new JPanel[getNumberOfQueuesValue()];
        clients = new JTextArea[getNumberOfQueuesValue()];
        for(int i = 0; i < getNumberOfQueuesValue(); i++) {
            qs[i] = new JPanel();
            qs[i].setLayout(new BoxLayout(qs[i], BoxLayout.Y_AXIS));
            clients[i] = new JTextArea("Queue " + (i + 1));
            clients[i].setFont(new Font(clients[i].getFont().getFontName(), clients[i].getFont().getStyle(), 15));
            qs[i].add(clients[i]);
            qs[i].setBorder(BorderFactory.createLineBorder(Color.black));
            qs[i].setBackground(Color.white);
            c.gridx = i*2;
            displayingPanel.add(qs[i], c);
        }
        c.weighty = 0.1; c.weightx = 1; c.gridy = 1; c.gridx = 0; c.gridwidth = GridBagConstraints.REMAINDER; c.gridheight = 1;
        displayingPanel.add(timeOfSimulationPanel, c);
        timeOfSimulationPanel.setLayout(new BoxLayout(timeOfSimulationPanel, BoxLayout.Y_AXIS));
        timeOfSimulationPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        timeOfSimulationPanel.setBackground(Color.white);
        waitingClients = new JTextArea("Waiting clients: ");
        waitingClients.setLineWrap(true);
        timeOfSimulationPanel.add(waitingClients);
    }

    private JPanel buildSetupPanel() {
        numberOfQueuesField = new JTextField();
        numberOfClientsField = new JTextField();
        timeLimitField = new JTextField();
        minArrivalTimeField = new JTextField();
        maxArrivalTimeField = new JTextField();
        minServiceTimeField = new JTextField();
        maxServiceTimeField = new JTextField();
        submitSettingsButton = new JButton("Begin Simulation");
        String[] opt = new String[]{"fewest people", "shortest waiting time"};
        selectStrategy = new JComboBox(opt);
        FormLayout layout = new FormLayout(
                "right:pref, 3dlu, pref, 7dlu, right:pref, 3dlu, pref", // columns
                "p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p, 3dlu, p"); // rows
        layout.setColumnGroups(new int[][]{{1, 5}, {3, 7}});
        PanelBuilder builder = new PanelBuilder(layout);
        CellConstraints cc = new CellConstraints();
        builder.addSeparator("Simulation settings", cc.xyw(1,  1, 7));
        builder.addLabel("Number of clients", cc.xy (1,  3));
        builder.add(numberOfClientsField, cc.xyw(3, 3, 5));
        builder.addLabel("Number of queues", cc.xy (1,  5));
        builder.add(numberOfQueuesField, cc.xyw(3, 5, 5));
        builder.addLabel("Maximum simulation time", cc.xy (1,  7));
        builder.add(timeLimitField, cc.xyw(3, 7, 5));
        builder.addLabel("Minimum time of arrival", cc.xy(1, 9));
        builder.add(minArrivalTimeField, cc.xyw(3, 9, 5));
        builder.addLabel("Maximum time of arrival", cc.xy (1, 11));
        builder.add(maxArrivalTimeField, cc.xyw(3, 11, 5));
        builder.addLabel("Minimum service time", cc.xy (1, 13));
        builder.add(minServiceTimeField, cc.xyw(3, 13, 5));
        builder.addLabel("Maximum service time", cc.xy (1, 15));
        builder.add(maxServiceTimeField, cc.xyw(3, 15, 5));
        builder.addLabel("New client go to the queue with", cc.xy(1, 17));
        builder.add(selectStrategy, cc.xyw(3, 17, 5));
        builder.add(submitSettingsButton, cc.xyw(1, 19, 7));
        return builder.getPanel();
    }

    private void createGUI() {
        mainPanel = new JTabbedPane();
        simulationPanel = new JPanel();
        displayingPanel = new JPanel();
        this.setTitle("Queue Simulator");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(500, 400);
        this.setResizable(false);
        this.setLocationRelativeTo(null);
        simulationPanel.add(buildSetupPanel());
        mainPanel.addTab("Simulation Setup", null, simulationPanel, "Set the simulation parameters.");
        mainPanel.addTab("Displaying Simulation", null, displayingPanel, "See the simulation running.");
        mainPanel.setEnabledAt(1, false);
        mainPanel.setForegroundAt(0, Color.black);
        mainPanel.setForegroundAt(1, Color.black);
        this.setContentPane(mainPanel);
        this.setVisible(true);
    }
    public void updateQueue(String output, int index) {
        clients[index - 1].setText("Queue " + index + "\n" + output);
    }
    public void updateWaitingClients(String output) {
        waitingClients.setText(output);
    }
    public void prepareForDisplaying() {
        mainPanel.setSelectedIndex(1);
        mainPanel.setEnabledAt(0, false);
        updateDisplayingPanel();
        resizeFrame();
    }
    private void resizeFrame() {
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        if(getNumberOfQueuesValue() * 100 > size.width)
            this.setSize(size.width, size.height);
        else this.setSize(getNumberOfQueuesValue() * 100, 600);
        this.setLocationRelativeTo(null);
    }
    public void updateCurrentTime(int currentTime) {
        this.setTitle("Time " + currentTime);
    }
    public int getTimeLimitValue() {
        return Integer.parseInt(timeLimitField.getText());
    }
    public int getNumberOfQueuesValue() {
        return Integer.parseInt(numberOfQueuesField.getText());
    }
    public int getNumberOfClientsValue() {
        return Integer.parseInt(numberOfClientsField.getText());
    }
    public int getMinArrivalTimeValue() {
        return Integer.parseInt(minArrivalTimeField.getText());
    }
    public int getMaxArrivalTimeValue() {
        return Integer.parseInt(maxArrivalTimeField.getText());
    }
    public int getMinServiceTimeValue() {
        return Integer.parseInt(minServiceTimeField.getText());
    }
    public int getMaxServiceTimeValue() {
        return Integer.parseInt(maxServiceTimeField.getText());
    }
    public Scheduler.SelectionPolicy getStrategy() {
        if(selectStrategy.getSelectedIndex() == 0)
            return Scheduler.SelectionPolicy.SHORTEST_QUEUE;
        return Scheduler.SelectionPolicy.SHORTEST_TIME;
    }
    public JButton getSubmitSettingsButton() {
        return submitSettingsButton;
    }
    public JTextArea getWaitingClients() {
        return waitingClients;
    }
}
