package ro.tuc.pt.tema2.DataModels;
import ro.tuc.pt.tema2.Logic.Strategy;
import ro.tuc.pt.tema2.Logic.StrategyQueue;
import ro.tuc.pt.tema2.Logic.StrategyTime;

import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private final List<Queue> qs;
    private int maxQueues;
    private int maxClientsPerQueue;
    private Strategy strategy;
    private Thread[] qt;

    public Scheduler(int maxQueues, int maxClientsPerQueue) {
        this.maxQueues = maxQueues;
        this.maxClientsPerQueue = maxClientsPerQueue;
        qt = new Thread[this.maxQueues];
        qs = new ArrayList<>();
        for(int i = 0; i < this.maxQueues; i++) {
            qs.add(new Queue());
            qt[i] = new Thread(qs.get(i));
            qt[i].start();
        }
    }

    public void changeStrategy(SelectionPolicy policy) {
        if(policy == SelectionPolicy.SHORTEST_TIME)
            strategy = new StrategyTime();
        if(policy == SelectionPolicy.SHORTEST_QUEUE)
            strategy = new StrategyQueue();
    }

    public void dispatchClient(Client c) {
        strategy.addClient(qs, c, this.maxClientsPerQueue);
    }

    public List<Queue> getQueues() {
        return qs;
    }

    public enum SelectionPolicy {
        SHORTEST_QUEUE, SHORTEST_TIME
    }
}
