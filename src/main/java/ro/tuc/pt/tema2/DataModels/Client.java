package ro.tuc.pt.tema2.DataModels;

public class Client implements Comparable<Client>{
    private Integer ID;
    private Integer arrivalTime;
    private Integer serviceTime;
    private Integer waitingTime;

    public Client(int ID, int arrivalTime, int serviceTime) {
        this.ID = ID;
        this.arrivalTime = arrivalTime;
        this.serviceTime = serviceTime;
        this.waitingTime = 0;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }
    public int getServiceTime() {
        return serviceTime;
    }
    public int getWaitingTime() {
        return waitingTime;
    }
    public void decrementServiceTime() {
        this.serviceTime--;
    }
    public void incrementWaitingTime() {
        this.waitingTime++;
    }

    @Override
    public int compareTo(Client o) {
        return this.arrivalTime.compareTo(o.arrivalTime);
    }

    public String toString() {
        return "(" + ID + ", " + arrivalTime + ", " + serviceTime + ")";
    }
}
