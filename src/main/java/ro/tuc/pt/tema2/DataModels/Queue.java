package ro.tuc.pt.tema2.DataModels;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable{
    private BlockingQueue<Client> clients;
    private AtomicInteger waitingTime;
    private Integer totalNumberOfClients;
    private Integer totalWaitingTime;
    private Integer totalServiceTime;
    private boolean cancel;

    public Queue() {
        cancel = false;
        clients = new LinkedBlockingQueue<>();
        waitingTime = new AtomicInteger();
        waitingTime.set(0);
        totalNumberOfClients = 0;
        totalServiceTime = 0;
        totalWaitingTime = 0;
    }

    public void addClient(Client c) {
        clients.add(c);
        totalNumberOfClients++;
        totalServiceTime += c.getServiceTime();
        waitingTime.addAndGet(c.getServiceTime());
    }

    @Override
    public void run() {
        while (!cancel) {
            try {
                Client client;
                if(!clients.isEmpty()) {
                    client = clients.element();
                    int serviceTime = client.getServiceTime();
                    for (int i = 0; i < serviceTime; i++) {
                        Thread.sleep(1000);
                        client.decrementServiceTime();
                        waitingTime.decrementAndGet();
                        for(Client c : clients)
                            if(!c.equals(client))
                                c.incrementWaitingTime();
                    }
                    if(client.getServiceTime() == 0) {
                        totalWaitingTime += client.getWaitingTime();
                        clients.take();
                    }
                }
            } catch (InterruptedException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
        }
    }

    public BlockingQueue<Client> getClients() {
        return clients;
    }
    public AtomicInteger getWaitingTime() {
        return waitingTime;
    }
    public Integer getTotalWaitingTime() {
        return totalWaitingTime;
    }
    public Integer getTotalServiceTime() {
        return totalServiceTime;
    }
    public void cancel() {
        cancel = true;
    }
    public boolean isEmpty() {
        if(clients.isEmpty())
            return true;
        return false;
    }
}
